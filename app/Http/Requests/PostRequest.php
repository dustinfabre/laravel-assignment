<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
        ];
    }


    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        return [
            'title.required' => 'Title must not be empty',
            'body.content' => 'Content must not be empty',
        ];
    }

    /**
    * Prepare the data for validation.
    *
    * @return void
    */
    // protected function prepareForValidation()
    // {
    //     $this->merge([
    //         'published_at' => $this->published_at != null ?? Carbon::parse($this->get('published_at')),
    //     ]);
    // }

    /**
    * Configure the validator instance.
    *
    * @param  \Illuminate\Validation\Validator  $validator
    * @return void
    */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $isPublishValid = $this->validatePublished();
            if ($isPublishValid != null) {
                $validator->errors()->add('publish_at', $isPublishValid);
            }
        });
    }

    private function validatePublished()
    {
        $valid = null;
        $this->published_at != null ?? Carbon::parse($this->published_at);
        switch ($this->status) {
            case 'planned':
                if ($this->published_at < Carbon::now()) {
                    $valid = 'The publishing date must be in the future';
                } elseif ($this->published_at == null) {
                    $valid = 'A planned post must have a publishing date';
                }
                break;
            case 'published':
                if ($this->published_at > Carbon::now()) {
                    $valid = 'A new post cannot be published in the future';
                } elseif ($this->published_at == null) {
                    $valid = 'A published post must have a publishing date';
                }
                break;
        }

        return $valid;

    }
}
