<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Post\Post;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('q')) {
            $posts = Post::where('title', 'LIKE', '%'.$request->get('q').'%');
        } else {
            $posts = Post::query();
        }

        if( $request->has('order')) {
            $posts = $posts->orderBy($request->get('orderby'), $request->get('order'));
        } else {
            $posts = $posts->orderBy('created_at', 'DESC');
        }

        $posts = $posts->get();

        return view('posts/index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts/new');
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {

        $post = new Post();
        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->status = $request->status;
        $post->content = $request->content;
        $post->published_at = $request->status == 'draft' ? null : $request->published_at;
        $post->save();

        @session_start();
        $_SESSION['post_message'] = 'Post succesfully created';

        return redirect()->route('posts.edit', $post);
    }

    /**
     * Display the specified resource.
     * @param  \App\Models\Post\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        if ($post->status === 'draft') {
            abort(404);
        } elseif ($post->status === 'planned') {
            if (! $post->published_at) {
                abort(404);
            } elseif ($post->published_at > Carbon::now()) {
                abort(404);
            }
        }

        return view('posts.view', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param  \App\Models\Post\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts/edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {

        if ($post->published_at && $request->published_at->equalTo($post->published_at)) {
            // If the given publishDate is equal to the current stored published_at
            if ($request->status === 'planned' && $request->published_at < Carbon::now()) {
                // Update post to published if the published_at date has passed
                $status = 'published';
            }
        }

        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->status = $request->status;
        $post->content = $request->content;
        $post->published_at = $request->status == 'draft' ? null : $request->published_at;
        $post->save();

        @session_start();
        $_SESSION['post_message'] = 'Post succesfully updated';

        return redirect()->route('posts.edit', $post);
    }

    /**
     * Remove the specified resource from storage.
     * @param  \App\Models\Post\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if (! $post->trashed()) {
            $post->delete();
        }

        @session_start();
        $_SESSION['post_message'] = 'Post "'.$post->title.'" has been deleted.';

        return redirect()->route('posts.index');
    }
}
